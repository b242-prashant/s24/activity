let getCube = 2**3;
console.log(`The cube of 2 is ${getCube}`);
let address = ["258 Washington Ave NW","California","90011"];
const [area, state, pincode] = address;
console.log(`I live in ${area} ${state} ${pincode}`);
let animal = {
	Name : "Lolong",
	Type : "saltwater crocodile",
	Weight : "1075 kgs",
	Measurement : "20 ft 3 in"
}

const {Name, Type, Weight, Measurement} = animal;

console.log(`${Name} was a ${Type}. He weighed at 1075 kgs with a measurement of ${Measurement}.`);

let numbers = [1,2,3,4,5];

numbers.forEach((number)=>{ console.log(number);})



let reducedNumber = numbers.reduce((x, y) => {
	return x + y;
})
console.log(reducedNumber);

function Dog(name, age, breed){
	// Properties
	this.name = name;
	this.age = age;
	this.breed = breed;
}

let frankie = new Dog("Frankie", 5, "Minitiature Dachshund");
console.log(frankie);




